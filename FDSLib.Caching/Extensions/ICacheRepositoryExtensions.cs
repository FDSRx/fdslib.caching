﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Helpers;


namespace FDSLib.Caching
{
    public static class ICacheRepositoryExtensions
    {

        /// <summary>
        /// Gets the value from the collection. Returns null if the attempt is unsuccessful.
        /// </summary>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="key">The name of the item to get from the collection.</param>
        public static object GetOrNull(this ICacheRepository repository, string key)
        {
            try
            {
                if (String.IsNullOrEmpty(key))
                {
                    return null;
                }

                if (repository.IsSet(key))
                {
                    return repository.Get(key);
                }
            }
            catch { }

            return null;
        }

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        /// <returns>A boolean indicating if the value was successfully set.</returns>
        public static bool TrySet(this ICacheRepository repository, string key, object value)
        {
            try
            {
                if (String.IsNullOrEmpty(key))
                {
                    return false;
                }

                repository.Set(key, value);

                return true;
            }
            catch {
                return false;
            }

            
        }

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        /// <returns></returns>
        public static bool TrySet(this ICacheRepository repository, CacheObject cacheItem)
        {
            try
            {
                if ((object)cacheItem == null)
                {
                    return false;
                }

                string key = cacheItem.Key;

                if (String.IsNullOrEmpty(key))
                {
                    return false;
                }


                if (repository.IsSet(key))
                {
                    repository.Remove(key);
                    repository.Add(cacheItem);
                }
                else
                {
                    repository.Add(cacheItem);
                }


                return true;
            }
            catch {
                return false;
            }

        }

        #region Generic retrieval methods

        /// <summary>
        /// Gets the value from the collection.
        /// </summary>
        /// <typeparam name="T">Specific object to retrieve from the collection.</typeparam>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        public static T Get<T>(this ICacheRepository repository, string key)
        {
            var obj = repository.Get(key);

            return ObjectHelper.ConvertToType<T>(obj);
        }

        /// <summary>
        /// Gets the value from the collection. Returns the default value if the reference object is null or does not exist.
        /// </summary>
        /// <typeparam name="T">Specific object to retrieve from the collection.</typeparam>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        public static T GetOrNew<T>(this ICacheRepository repository, string key) where T : class, new()
        {
            try
            {
                var obj = repository.Get(key);

                return obj == null ? new T() : ObjectHelper.ConvertToType<T>(obj);
            }
            catch (Exception)
            {
            }

            return new T();
        }

        /// <summary>
        /// Gets the value from the collection. Returns the default value if the reference object is null or does not exist.
        /// </summary>
        /// <typeparam name="T">Specific object to retrieve from the collection.</typeparam>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        public static T GetOrDefault<T>(this ICacheRepository repository, string key)
        {
            try
            {
                var obj = repository.Get(key);

                return obj == null ? default(T) : ObjectHelper.ConvertToType<T>(obj);
            }
            catch (Exception)
            {
            }

            return default(T);
        }

        /// <summary>
        /// Gets the value from the collection. Returns null if the type is unable to be cast.
        /// </summary>
        /// <typeparam name="T">Specific object to retrieve from the collection.</typeparam>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        public static T? GetOrNull<T>(this ICacheRepository repository, string key) where T : struct
        {
            try
            {
                var obj = repository.Get(key);

                return obj == null ? (T?)null : ObjectHelper.ConvertToType<T>(obj);
            }
            catch (Exception)
            {
            }

            return null;
        }

        #endregion

        /// <summary>
        /// Clears all items from the collection except for the specified key.
        /// </summary>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="key">Key to ignore.</param>
        public static void ClearExcept(this ICacheRepository repository, string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            try
            {
                //iterate over copy to remove from source
                foreach (var id in repository.Keys)
                {
                    if (id == key)
                    {
                        //key removal exception was found.  Let's ignore and continue.
                        continue;
                    }

                    repository.Remove(key);
                }
            }
            catch { }
        }

        /// <summary>
        /// Clears all items from the collection except for the specified keys.
        /// </summary>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="keys">List of keys to ignore.</param>
        public static void ClearExcept(this ICacheRepository repository, IList<string> keys)
        {
            if (keys == null)
            {
                throw new ArgumentNullException("keys");
            }

            try
            {
                if (keys != null && keys.Count > 0)
                {
                    //iterate over copy to remove from source
                    foreach (var key in repository.Keys)
                    {
                        if (keys.Contains(key))
                        {
                            //key removal exception was found.  Let's ignore and continue.
                            continue;
                        }

                        //remove key
                        repository.Remove(key);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Clears all items from the collection except for the specified keys.
        /// </summary>
        /// <param name="repository">ICacheRepository object.</param>
        /// <param name="keys">string array of keys to ignore.</param>
        public static void ClearExcept(this ICacheRepository repository, string[] keys)
        {
            ClearExcept(repository, keys.ToList());
        }

       
    }
}
