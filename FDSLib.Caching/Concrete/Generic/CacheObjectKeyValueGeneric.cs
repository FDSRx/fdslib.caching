﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace FDSLib.Caching
{
    /// <summary>
    /// Cache item object.
    /// </summary>
    /// <typeparam name="TKey">The type of key used to identify the cache item (e.g. string, int, etc.).</typeparam>
    /// <typeparam name="TValue">The type of cache item.</typeparam>
    public class CacheObject<TKey, TValue> : ICacheObject<TKey, TValue>, IDisposable, ICloneable
    {

        #region ICacheObject Members

        /// <summary>
        /// Unique identifier of cache object.
        /// </summary>
        public TKey Key { get; private set; }

        /// <summary>
        /// Object item.
        /// </summary>
        public TValue Value { get; private set; }

        /// <summary>
        ///The date/time the cache object will expire.
        /// </summary>
        public DateTime ExpirationDate { get; private set; }

        /// <summary>
        /// The policy of the object.
        /// </summary>
        public CacheObjectPolicy Policy { get; set; }

        /// <summary>
        /// The date/time the cache object was created.
        /// </summary>
        public DateTime DateCreated { get; private set; }

        /// <summary>
        /// The date/time the cache object was modified.
        /// </summary>
        public DateTime DateModified { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Instantiates a new cache object.
        /// </summary>
        /// <param name="key">Unique identifier of cache object.</param>
        /// <param name="value">Object item.</param>
        public CacheObject(TKey key, TValue value)
        {
            //check for null reference to the object.
            if ((object)key == null)
            {
                throw new ArgumentNullException("key");
            }

            this.Key = key;
            this.Value = value;
            this.DateCreated = DateTime.Now;
            this.DateModified = DateTime.Now;
            this.ExpirationDate = DateTime.MaxValue;
            this.Policy = new CacheObjectPolicy();
        }

        /// <summary>
        /// Instantiates a new cache object.
        /// </summary>
        /// <param name="key">Unique identifier of cache object.</param>
        /// <param name="value">object item.</param>
        /// <param name="duration">The amount of time the item is allowed to remain cached.</param>
        public CacheObject(TKey key, TValue value, TimeSpan duration) :
            this(key, value)
        {
            this.ExpirationDate = DateTime.Now.Add(duration);
        }

        #endregion

        /// <summary>
        /// Indicates if the cache item has expired.
        /// </summary>
        public bool IsExpired
        {
            get
            {
                return this.ExpirationDate < DateTime.Now;
            }
        }

        /// <summary>
        /// Sets the cache item to a new value.
        /// </summary>
        /// <param name="value">Object item.</param>
        public void Set(TValue value)
        {
            this.Value = value;
            this.DateModified = DateTime.Now;
        }

        /// <summary>
        /// Sets the cache item to a new value.
        /// </summary>
        /// <param name="value">Object item.</param>
        /// <param name="duration">The amount of time the item is allowed to remain in the cache repository.</param>
        public void Set(TValue value, TimeSpan duration)
        {
            this.Value = value;
            this.ExpirationDate = DateTime.Now.Add(duration);
            this.Policy.Expiration = DateTime.Now.Add(duration);
            this.DateModified = DateTime.Now;
        }

        #region IDisposable Members

        /// <summary>
        /// Disposes of the cache object.
        /// </summary>
        /// <param name="disposing">Indicates whether the object should be disposed.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Value = default(TValue);
                this.DateModified = DateTime.Now;
                this.ExpirationDate = DateTime.MinValue;
                this.Policy.Expiration = null;
            }
        }

        /// <summary>
        /// Disposes of the cahce object.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Dispose(true);
        }

        #endregion

        #region ICloneable Members

        /// <summary>
        /// Creates a shallow copy of the CacheObject item.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return this.ShallowCopy();
        }

        /// <summary>
        /// Creates a shallow copy of the CacheObject item.
        /// </summary>
        /// <returns></returns>
        public TValue ShallowCopy()
        {
            return (TValue)this.MemberwiseClone();
        }

        /// <summary>
        /// Creates a deep copy of the CacheItem object.
        /// </summary>
        /// <returns></returns>
        public TValue DeepCopy()
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();

            bf.Serialize(ms, this);
            ms.Flush();
            ms.Position = 0;

            var clone = (TValue)bf.Deserialize(ms);
            ms.Close();
            ms.Dispose();

            return clone;
        }

        #endregion

        /// <summary>
        /// Returns the key name of the cache object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Key.ToString();
        }
    }
}
