﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    public class CacheObjectPolicy
    {
        /// <summary>
        /// Gets or sets a value that indicates whether a cache entry should be evicted after a specified duration.
        /// </summary>
        public DateTimeOffset? Expiration { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CacheObjectPolicy object.
        /// </summary>
        public CacheObjectPolicy()
        {

        }

        #endregion

        /// <summary>
        /// Indicates if the object policy has expired.
        /// </summary>
        public bool IsExpired
        {
            get
            {
                return this.Expiration < DateTime.Now;
            }
        }
    }
}
