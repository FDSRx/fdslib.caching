﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    /// <summary>
    /// Object based caching mechanism.
    /// </summary>
    public interface ICacheObject
    {
        /// <summary>
        /// Unique identifier of cache object.
        /// </summary>
        string Key { get; }

        /// <summary>
        /// Object item.
        /// </summary>
        object Value { get; }

        /// <summary>
        ///The date/time the cache object will expire.
        /// </summary>
        DateTime ExpirationDate { get; }

        /// <summary>
        /// The policy of the object.
        /// </summary>
        CacheObjectPolicy Policy { get; set; }

        /// <summary>
        /// The date/time the cache object was created.
        /// </summary>
        DateTime DateCreated { get; }

        /// <summary>
        /// The date/time the cache object was modified.
        /// </summary>
        DateTime DateModified { get; }
    }



}
