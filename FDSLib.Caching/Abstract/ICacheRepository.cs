﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    public interface ICacheRepository
    {
        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        void Add(string key, object value);

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        void Add(CacheObject cacheItem);

        /// <summary>
        /// Gets the value from the collection.
        /// </summary>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        object Get(string key);

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        /// <returns></returns>
        void Set(string key, object value);

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        void Set(CacheObject cacheItem);

        /// <summary>
        /// Indicates whether the object exists in the collection.
        /// </summary>
        /// <param name="key">The name of the item to validate in the collection.</param>
        /// <returns></returns>
        bool IsSet(string key);

        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Deletes an item from the collection.
        /// </summary>
        /// <param name="key">The name of the item to remove from the collection.</param>
        void Remove(string key);

        /// <summary>
        /// Removes all keys and values from the collection.
        /// </summary>
        void Clear();

        /// <summary>
        /// Returns a list keys within the collection.
        /// </summary>
        IList<string> Keys { get; }

    }
}
