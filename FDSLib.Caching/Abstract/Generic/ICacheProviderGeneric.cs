﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    /// <summary>
    /// Cache provider contract.
    /// </summary>
    /// <typeparam name="TKey">The type of key used to identify the cache item (e.g. string, int, etc.).</typeparam>
    /// <typeparam name="TValue">The type of cache item.</typeparam>
    public interface ICacheProvider<in TKey, TValue>
    {
        /// <summary>
        /// Gets value associated with cache key.
        /// </summary>
        /// <param name="key">Cache key.</param>
        TValue Get(TKey key);

        /// <summary>
        /// Adds the item to the cahce.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">The item to cache.</param>
        void Add(TKey key, TValue value);

        /// <summary>
        /// Adds/Updates the cache object.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">The item to cache.</param>
        void Set(TKey key, TValue value);

        /// <summary>
        /// Sets cache object.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">Object to cache.</param>
        /// <param name="duration">The amount of time the item is allowed to remain cached.</param>
        void Set(TKey key, TValue value, TimeSpan duration);

        /// <summary>
        /// Returns true if the key exists or false if the key does not exist.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <returns></returns>
        bool IsSet(TKey key);

        /// <summary>
        /// Removes item from the cache.
        /// </summary>
        /// <param name="key">Cache key.</param>
        void Remove(TKey key);

        /// <summary>
        /// Clears the cache of all entries.
        /// </summary>
        void Clear();
    }
}
