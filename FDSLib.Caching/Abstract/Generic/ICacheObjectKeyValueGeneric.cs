﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    /// <summary>
    /// Generic cache provider.
    /// </summary>
    /// <typeparam name="TKey">The type of key used to store the object (e.g. string, int, decimal, etc.).</typeparam>
    /// <typeparam name="TValue">The type of object to be stored (e.g. string, int, etc.)</typeparam>
    public interface ICacheObject<out TKey, out TValue>
    {
        /// <summary>
        /// Unique identifier of cache object.
        /// </summary>
        TKey Key { get; }

        /// <summary>
        /// Object item.
        /// </summary>
        TValue Value { get; }

        /// <summary>
        ///The date/time the cache object will expire.
        /// </summary>
        DateTime ExpirationDate { get; }

        /// <summary>
        /// The policy of the object.
        /// </summary>
        CacheObjectPolicy Policy { get; set; }

        /// <summary>
        /// The date/time the cache object was created.
        /// </summary>
        DateTime DateCreated { get; }

        /// <summary>
        /// The date/time the cache object was modified.
        /// </summary>
        DateTime DateModified { get; }
    }
}
