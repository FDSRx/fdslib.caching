﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    public interface ICacheRepository<TKey, TValue>
    {
        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        void Add(TKey key, TValue value);

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        void Add(CacheObject<TKey, TValue> cacheItem);

        /// <summary>
        /// Gets the value from the collection.
        /// </summary>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        TValue Get(TKey key);

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        /// <returns></returns>
        void Set(TKey key, TValue value);

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        void Set(CacheObject<TKey, TValue> cacheItem);

        /// <summary>
        /// Indicates whether the object exists in the collection.
        /// </summary>
        /// <param name="key">The name of the item to validate in the collection.</param>
        /// <returns></returns>
        bool IsSet(TKey key);

        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Deletes an item from the collection.
        /// </summary>
        /// <param name="key">The name of the item to remove from the collection.</param>
        void Remove(TKey key);

        /// <summary>
        /// Removes all keys and values from the collection.
        /// </summary>
        void Clear();

        /// <summary>
        /// Returns a list keys within the collection.
        /// </summary>
        IList<TKey> Keys { get; }

    }
}
