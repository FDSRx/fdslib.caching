﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    /// <summary>
    /// Cache provider contract.
    /// </summary>
    public interface ICacheProvider
    {

        /// <summary>
        /// Gets value associated with cache key.
        /// </summary>
        /// <param name="key">Cache key.</param>
        object Get(string key);

        /// <summary>
        /// Adds the item to the cache.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">The item to cache.</param>
        void Add(string key, object value);

        /// <summary>
        /// Sets cache object.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">The item to cache.</param>
        void Set(string key, object value);

        /// <summary>
        /// Sets cache object.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">Object to cache.</param>
        /// <param name="duration">The amount of time the item is allowed to remain cached.</param>
        void Set(string key, object value, TimeSpan duration);

        /// <summary>
        /// Returns true if the key exists or false if the key does not exist.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <returns></returns>
        bool IsSet(string key);

        /// <summary>
        /// Removes item from the cache.
        /// </summary>
        /// <param name="key">Cache key.</param>
        void Remove(string key);

        /// <summary>
        /// Clears the cache of all entries.
        /// </summary>
        void Clear();
    }
}
