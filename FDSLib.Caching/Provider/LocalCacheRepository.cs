﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Caching
{
    /// <summary>
    /// Utilizes a standard Dictionary(string, object) to cache information within the context of the current process being performed.
    /// </summary>
    public class LocalCacheRepository : ICacheRepository
    {
        /// <summary>
        /// Cache object.
        /// </summary>
        private readonly Dictionary<string, object> _repository = new Dictionary<string, object>();

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        public void Add(string key, object value)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            this._repository.Add(key, value);
        }

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        public void Add(CacheObject cacheItem)
        {
            if ((object)cacheItem == null)
            {
                throw new ArgumentNullException("cacheItem");
            }

            string key = cacheItem.Key;
            object value = cacheItem.Value;

            this.Add(key, value);
        }

        /// <summary>
        /// Gets the value from the collection.
        /// </summary>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        public object Get(string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            return this._repository[key];
        }

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        /// <returns></returns>
        public void Set(string key, object value)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            if (this._repository.ContainsKey(key))
            {
                this._repository[key] = value;
            }
            else
            {
                this._repository.Add(key, value);
            }
        }

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        public void Set(CacheObject cacheItem)
        {
            if ((object)cacheItem == null)
            {
                throw new ArgumentNullException("cacheItem");
            }

            string key = cacheItem.Key;
            object value = cacheItem.Value;

            this.Set(key, value);
        }

        /// <summary>
        /// Indicates whether the object exists in the collection.
        /// </summary>
        /// <param name="key">The name of the item to validate in the collection.</param>
        /// <returns></returns>
        public bool IsSet(string key)
        {
            if (this._repository.ContainsKey(key))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        public int Count
        {
            get { return this._repository.Count; }
        }

        /// <summary>
        /// Deletes an item from the collection.
        /// </summary>
        /// <param name="key">The name of the item to remove from the collection.</param>
        public void Remove(string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            if (this.IsSet(key))
                this._repository.Remove(key);
        }

        /// <summary>
        /// Removes all keys and values from the collection.
        /// </summary>
        public void Clear()
        {
            this._repository.Clear();
        }

        /// <summary>
        /// Returns a list keys within the collection.
        /// </summary>
        public IList<string> Keys
        {
            get { return _repository.Select(kvp => kvp.Key).ToList(); }
        }
    }
}
