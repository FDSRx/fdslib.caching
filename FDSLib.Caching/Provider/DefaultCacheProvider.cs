﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching;
using System.Runtime.Serialization;

namespace FDSLib.Caching
{
    public class DefaultCacheProvider : ICacheProvider
    {
        /// <summary>
        /// Cache object.
        /// </summary>
        private ObjectCache Cache { get { return MemoryCache.Default; } }

        /// <summary>
        /// Gets value associated with cache key.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <returns></returns>
        public object Get(string key)
        {
            return Cache[key];
        }

        /// <summary>
        /// Adds the item to the cache.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">The item to cache.</param>
        public void Add(string key, object value)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }
            
            try
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTime.MaxValue;

                Cache.Add(new CacheItem(key, value), policy);
            }
            catch { }
        }

        /// <summary>
        /// Sets cache object.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">The item to cache.</param>
        public void Set(string key, object value)
        {
            this.Set(key, value, new TimeSpan(365, 0, 0, 0, 0));
        }

        /// <summary>
        /// Sets cache object.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <param name="value">Object to cache.</param>
        /// <param name="duration">The amount of time an item is allowed to remain cached.</param>
        public void Set(string key, object value, TimeSpan duration)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            try
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTime.Now.Add(duration);

                if (this.IsSet(key))
                    this.Remove(key);

                Cache.Add(new CacheItem(key, value), policy);
            }
            catch { }
        }

        /// <summary>
        /// Returns true if the key exists or false if the key does not exist.
        /// </summary>
        /// <param name="key">Cache key.</param>
        /// <returns></returns>
        public bool IsSet(string key)
        {
            return (Cache[key] != null);
        }

        /// <summary>
        /// Removes item from the cache.
        /// </summary>
        /// <param name="key">Cache key.</param>
        public void Remove(string key)
        {
            try
            {
                if (this.IsSet(key))
                    Cache.Remove(key);
            }
            catch { }
        }

        /// <summary>
        /// Clears the cache of all entries.
        /// </summary>
        public void Clear()
        {
            try
            {
                List<string> cacheKeys = Cache.Select(kvp => kvp.Key).ToList();

                foreach (string cacheKey in cacheKeys)
                {
                    Cache.Remove(cacheKey);
                }

                //MemoryCache.Default.Dispose();
            }
            catch { }
        }
    }
}
