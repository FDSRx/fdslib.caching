﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching;
using System.Runtime.Serialization;

namespace FDSLib.Caching
{
    /// <summary>
    /// Utilizes system.Runtime.Caching to cache elements that can be shared across sessions.
    /// </summary>
    public class GlobalCacheRepository : ICacheRepository
    {
        /// <summary>
        /// Cache object.
        /// </summary>
        private ObjectCache _repository { get { return MemoryCache.Default; } }

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        public void Add(string key, object value)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(2880); //default to 48 hours.

            this._repository.Add(new CacheItem(key, value), policy); 
        }

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        public void Add(CacheObject cacheItem)
        {
            if ((object)cacheItem == null)
            {
                throw new ArgumentNullException("cacheItem");
            }

            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTime.Now + (new TimeSpan(48, 0, 0)); //default to 48 hours.

            this._repository.Add(new CacheItem(cacheItem.Key, cacheItem.Value), policy);
        }

        /// <summary>
        /// Gets the value from the collection.
        /// </summary>
        /// <param name="key">The name of the item to get from the collection.</param>
        /// <returns></returns>
        public object Get(string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            return this._repository[key];
        }

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="key">The name of the item to add to the collection.</param>
        /// <param name="value">The value of the item to add to the collection.</param>
        /// <returns></returns>
        public void Set(string key, object value)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            if (this.IsSet(key))
            {
                this._repository[key] = value;
            }
            else
            {
                this.Add(key, value);
            }
        }

        /// <summary>
        /// Adds/Overwrites an item in the collection.
        /// </summary>
        /// <param name="cacheItem">Cache item to be added to the collection.</param>
        public void Set(CacheObject cacheItem)
        {
            if ((object)cacheItem == null)
            {
                throw new ArgumentNullException("cacheItem");
            }

            if (String.IsNullOrEmpty(cacheItem.Key))
            {
                throw new ArgumentNullException("cacheItem.key");
            }

            if (this.IsSet(cacheItem.Key))
            {
                this._repository[cacheItem.Key] = cacheItem.Value;
            }
            else
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = cacheItem.Policy.Expiration ?? (DateTime.Now + (new TimeSpan(48, 0, 0))); //default to 48 hours.

                this._repository.Add(new CacheItem(cacheItem.Key, cacheItem.Value), policy);
            }
        }

        /// <summary>
        /// Indicates whether the object exists in the collection.
        /// </summary>
        /// <param name="key">The name of the item to validate in the collection.</param>
        /// <returns></returns>
        public bool IsSet(string key)
        {
            return (_repository[key] != null);
        }

        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        public int Count
        {
            get { return this._repository.Count(); }
        }

        /// <summary>
        /// Deletes an item from the collection.
        /// </summary>
        /// <param name="key">The name of the item to remove from the collection.</param>
        public void Remove(string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            if (this.IsSet(key))
                this._repository.Remove(key);
        }

        /// <summary>
        /// Removes all keys and values from the collection.
        /// </summary>
        public void Clear()
        {
            try
            {
                List<string> cacheKeys = _repository.Select(kvp => kvp.Key).ToList();

                foreach (string cacheKey in cacheKeys)
                {
                    _repository.Remove(cacheKey);
                }

                //MemoryCache.Default.Dispose();
            }
            catch { }
        }

        /// <summary>
        /// Returns a list keys within the collection.
        /// </summary>
        public IList<string> Keys
        {
            get { return _repository.Select(kvp => kvp.Key).ToList(); }
        }
    }
}
